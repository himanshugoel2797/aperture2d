﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aperture2D.WinForms
{
    public partial class GameWindow : Form
    {
        Graphics graphics, mainGraphics, rttGraphics;
        Bitmap backBuffer, rtt;
        Bitmap[] curSetTextures;
        Keys k;

        public GameWindow()
        {
            InitializeComponent();

            curSetTextures = new Bitmap[5];
            backBuffer = new Bitmap((int)EngineCore.Screen.X, (int)EngineCore.Screen.Y);
            this.BackgroundImage = new Bitmap((int)EngineCore.Screen.X, (int)EngineCore.Screen.Y);
            rtt = new Bitmap((int)EngineCore.Screen.X, (int)EngineCore.Screen.Y);
            graphics = Graphics.FromImage(backBuffer);
            mainGraphics = Graphics.FromImage(this.BackgroundImage);
            rttGraphics = Graphics.FromImage(rtt);

            #region EngineCore Initialization
            EngineCore.SetClear((r, g, b, a) =>
            {
                graphics.Clear(Color.FromArgb(a, r, g, b));
            });

            EngineCore.SetSwapBuffer(() =>
            {
                this.CreateGraphics().DrawImage(backBuffer, 0, 0, this.ClientSize.Width, this.ClientSize.Height);
            });

            EngineCore.SetDrawFunction((x, y, offsetX, offsetY, width, height) =>
            {
                if (offsetY + height > curSetTextures[0].Height | offsetX + width > curSetTextures[0].Width) return;
                graphics.DrawImage(
                    curSetTextures[0].Clone(
                    new Rectangle((int)offsetX, (int)offsetY, (int)width, (int)height), System.Drawing.Imaging.PixelFormat.DontCare)
                    , x, y, width, height);
            });

            EngineCore.SetTextureLoader((path) =>
            {
                return new Bitmap(path);
            });

            EngineCore.SetTextureDisposer((bmp) =>
            {
                (bmp as Bitmap).Dispose();
            });

            EngineCore.SetTextureSetter((bmp, num) =>
            {
                curSetTextures[num] = bmp as Bitmap;
            });

            EngineCore.SetTransparentColorSetter((tex, r, g, b) =>
            {
                (tex as Bitmap).MakeTransparent(Color.FromArgb(r, g, b));
                return tex;
            });

            EngineCore.SetColorSubtracter((tex, r, g, b) =>
                {
                    Color toSub = Color.FromArgb(r, g, b);
                    Bitmap tmp = (tex as Bitmap);
                    for(int y = 0; y < tmp.Height; y++)
                    {
                        for(int x = 0; x < tmp.Width; x++)
                        {
                            Color pixel = tmp.GetPixel(x, y);
                            int red = Math.Min(Math.Max(pixel.R - r, 0), 255);
                            int green = Math.Min(Math.Max(pixel.G - g, 0), 255);
                            int blue = Math.Min(Math.Max(pixel.B - b, 0), 255);
                            //int alpha = (red == pixel.R) ? pixel.R : pixel.A - red;
                            tmp.SetPixel(x, y, Color.FromArgb(pixel.G, red, green, blue));
                        }
                    }
                    return tmp;
                });

            EngineCore.SetBindRenderTexture((i) =>
                {
                    curSetTextures[i] = rtt;
                });

            EngineCore.SetClearRenderTexture((r, g, b, a) =>
                {
                    rttGraphics.Clear(Color.FromArgb(a, r, g, b));
                });

            EngineCore.SetRenderToTexture((x, y, offsetX, offsetY, width, height) =>
            {
                if (offsetY + height > curSetTextures[0].Height | offsetX + width > curSetTextures[0].Width) return;
                rttGraphics.DrawImage(
                    curSetTextures[0].Clone(
                    new Rectangle((int)offsetX, (int)offsetY, (int)width, (int)height), System.Drawing.Imaging.PixelFormat.DontCare)
                    , x, y, width, height);
            });
            #endregion

            #region Input Initialization
            Input.SetUpdater(() =>
            {
                InputKeys iKeys = new InputKeys();
                if (k.HasFlag(Keys.Right)) iKeys |= InputKeys.Right;
                else if (k.HasFlag(Keys.Up)) iKeys |= InputKeys.Up;
                else if (k.HasFlag(Keys.Down)) iKeys |= InputKeys.Down;
                else if (k.HasFlag(Keys.Left)) iKeys |= InputKeys.Left;

                if (k.HasFlag(Keys.Escape)) iKeys |= InputKeys.Start;
                if (k.HasFlag(Keys.Delete)) iKeys |= InputKeys.Select;

                if (k.HasFlag(Keys.ShiftKey)) iKeys |= InputKeys.Square;
                if (k.HasFlag(Keys.ControlKey)) iKeys |= InputKeys.Triangle;

                if (k.HasFlag(Keys.Enter)) iKeys |= InputKeys.Enter;
                if (k.HasFlag(Keys.Back)) iKeys |= InputKeys.Back;

                k = new Keys();

                if (iKeys != InputKeys.Empty) iKeys &= ~InputKeys.Empty;

                return iKeys;
            });
            #endregion


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            EngineCore.GameLoop();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            k = e.KeyCode;
            return;
        }
    }
}
