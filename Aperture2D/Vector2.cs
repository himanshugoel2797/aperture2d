﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperture2D
{
    public struct Vector2
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vector2(float x, float y) : this()
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return "{ X = " + X.ToString() + " , Y = " + Y.ToString() + "}";
        }
    }
}
