﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperture2D
{
    public class EngineCore
    {
        internal static Action<float, float, float, float, float, float> DrawRect;
        internal static Func<string, object> LoadTexture;
        internal static Action<object> DisposeTexture;
        internal static Action<object, int> SetTexture;
        internal static Action<int, int, int, int> Clear;
        internal static Func<object, int, int, int, object> SetTransparentColor;
        internal static Func<object, int, int, int, object> SubtractColor;
        internal static Action SwapBuffer;
        internal static Action<float, float, float, float, float, float> RenderToTexture;
        internal static Action<int, int, int, int> ClearRenderTexture;
        internal static Action<int> BindRenderTexture;
        public static Action Render;
        public static Vector2 Screen = new Vector2(960,540);

        public static void SetRenderToTexture(Action<float, float, float, float, float, float> rtt)
        {
            RenderToTexture = rtt;
        }

        public static void SetClearRenderTexture(Action<int, int, int, int> clr)
        {
            ClearRenderTexture = clr;
        }

        public static void SetBindRenderTexture(Action<int> BindRenderTex)
        {
            BindRenderTexture = BindRenderTex;
        }

        /// <summary>
        /// Specify the Rectangle drawing function
        /// </summary>
        /// <param name="draw"></param>
        public static void SetDrawFunction(Action<float, float, float, float, float, float> draw)
        {
            DrawRect = draw;
        }

        public static void SetTextureLoader(Func<string, object> loadTex)
        {
            LoadTexture = loadTex;
        }

        public static void SetTextureSetter(Action<object, int> setTex)
        {
            SetTexture = setTex;
        }

        public static void SetTextureDisposer(Action<object> disposer)
        {
            DisposeTexture = disposer;
        }

        public static void SetClear(Action<int,int,int,int> clr)
        {
            Clear = clr;
        }

        public static void SetSwapBuffer(Action swap)
        {
            SwapBuffer = swap;
        }

        public static void SetTransparentColorSetter(Func<object, int, int, int, object> stc)
        {
            SetTransparentColor = stc;
        }

        public static void SetColorSubtracter(Func<object, int, int, int, object> stc)
        {
            SubtractColor = stc;
        }

        public static void Start()
        {
            /*if (DrawRect == null |
                LoadTexture == null |
                DisposeTexture == null |
                SetTexture == null |
                Clear == null |
                SwapBuffer == null) throw new Exception("All the necessary graphics delegates need to be set first");*/

            while(true)
            {
                GameLoop();
            }

        }

        public static void GameLoop()
        {
            Input.KeysPressed = Input.Update();
            Clear(0, 255, 255/2, 0);

            Render();

            SwapBuffer();
        }

    }
}
