﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperture2D
{
    [Flags]
    public enum InputKeys
    {
        Empty = 0,
        Up = 1 << 0,
        Down = 1 << 1,
        Left = 1 << 2,
        Right = 1 << 3,
        Enter = 1 << 4,
        Back = 1 << 5,
        Start = 1 << 6,
        Select = 1 << 7,
        Square = 1 << 8,
        Triangle = 1 << 9
    }

    public static class Input
    {
        public static InputKeys KeysPressed { get; internal set; }

        internal static Func<InputKeys> Update;
        public static void SetUpdater(Func<InputKeys> updater)
        {
            Update = updater;
        }
    }
}
