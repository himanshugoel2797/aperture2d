﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperture2D
{
    public class RPG
    {
        public Dictionary<string, Action<Dictionary<string, string>>> Events;
        Map map;
        Sprite charSprite;
        CharacterController character;
        Vector2 tileRes;
        Vector2 localPos;

        public Sprite Player { get { return charSprite; } set { charSprite = value; } }
        public Map Map { get { return map; } set { map = value; } }
        public CharacterController Controller { get { return character; } set { character = value; } }

        public RPG(string mapPath, string charac)
        {
            Events = new Dictionary<string, Action<Dictionary<string, string>>>();
            map = new Map(mapPath);
            map.LoadResources();
            charSprite = new Sprite(charac);
            character = new CharacterController(charSprite, map);

            tileRes = new Vector2((int)(EngineCore.Screen.X / map.TileSize.X),
                (int)(EngineCore.Screen.Y / map.TileSize.Y));

            localPos = new Vector2(0, 0);
        }

        public void Draw()
        {
            if (charSprite.Position.X > EngineCore.Screen.X - map.TileSize.X && localPos.X < map.TilesCount.X - tileRes.X)
            {
                localPos.X += tileRes.X;
                charSprite.Position = new Vector2(map.TileSize.X, charSprite.Position.Y);
                map.IsDirty(true);
            }
            else if (charSprite.Position.X > EngineCore.Screen.X - map.TileSize.X)
            {
                charSprite.Position = new Vector2(EngineCore.Screen.X - map.TileSize.X, charSprite.Position.Y);
            }

            Console.WriteLine("LocalPos" + localPos);
            Console.WriteLine("Map:" + map.TileSize);

            if (charSprite.Position.Y > EngineCore.Screen.Y - map.TileSize.Y && localPos.Y < map.TilesCount.Y - tileRes.Y)
            {
                localPos.Y += tileRes.Y;
                charSprite.Position = new Vector2(charSprite.Position.X, map.TileSize.Y);
                map.IsDirty(true);
            }
            else if (charSprite.Position.Y > EngineCore.Screen.Y - map.TileSize.Y) charSprite.Position = new Vector2(charSprite.Position.X, EngineCore.Screen.Y - map.TileSize.Y);

            if (charSprite.Position.X < map.TileSize.X && localPos.X > 0)
            {
                localPos.X -= tileRes.X;
                charSprite.Position = new Vector2(EngineCore.Screen.X - map.TileSize.X, charSprite.Position.Y);
                map.IsDirty(true);
            }
            else if (charSprite.Position.X < 0)
            {
                charSprite.Position = new Vector2(0, charSprite.Position.Y);
                Console.WriteLine(0);
            }

            if (charSprite.Position.Y < map.TileSize.Y && localPos.Y > 0)
            {
                localPos.Y -= tileRes.Y;
                charSprite.Position = new Vector2(charSprite.Position.X, EngineCore.Screen.Y - map.TileSize.Y);
                map.IsDirty(true);
            }
            else if (charSprite.Position.Y < 0) charSprite.Position = new Vector2(charSprite.Position.X, 0);

            Console.WriteLine(charSprite.Position);

            localPos.X = (localPos.X < 0) ? 0 : (localPos.X > (map.TilesCount.X - tileRes.X)) ? (int)(map.TilesCount.X - tileRes.X) : localPos.X;
            localPos.Y = (localPos.Y < 0) ? 0 : (localPos.Y > (map.TilesCount.Y - tileRes.Y)) ? (int)(map.TilesCount.Y - tileRes.Y) : localPos.Y;

            var tile = map.GetTileWithProperties(charSprite.Position.X + charSprite.Size.X/2, charSprite.Position.Y + charSprite.Size.Y/2);
            if (tile != null && Events.ContainsKey(tile.Properties["Event"])) Events[tile.Properties["Event"]](new Dictionary<string, string>(tile.Properties));
            map.LocalPos = localPos;
            map.Draw();
            character.Update();
        }

    }
}
