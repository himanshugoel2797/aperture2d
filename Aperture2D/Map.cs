﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;

namespace Aperture2D
{
    public class Map
    {
        TmxMap map;
        Dictionary<string, object> TileSets;

        public Vector2 MapSize;
        public Vector2 TileSize;
        public Vector2 TilesCount;
        public Vector2 LocalPos;

        internal bool dirty = true;

        public Map(string path)
        {
            map = new TmxMap(path);

            TileSets = new Dictionary<string, object>();
            MapSize = new Vector2(map.Width * map.TileWidth, map.Height * map.TileHeight);
            TileSize = new Vector2(map.TileWidth, map.TileHeight);
            TilesCount = new Vector2(map.Width, map.Height);
        }

        public void LoadResources()
        {
            foreach (TmxTileset t in map.Tilesets)
            {
                if (!t.Properties.ContainsKey("Collision"))
                {
                    TileSets[t.Name] = EngineCore.SetTransparentColor(Textures.GetTexture(t.Image.Source), t.Image.Trans.R, t.Image.Trans.G, t.Image.Trans.B);
                    Textures.SetTexture(t.Image.Source, TileSets[t.Name]);
                }
            }
        }

        private TmxTileset GetTileSetForTile(int tileIndex)
        {
            var tiledTileset = map.Tilesets.Where(t => t.FirstGid <= tileIndex).OrderByDescending(t => t.FirstGid).FirstOrDefault();
            if (tiledTileset == null)
            {
                // Try to search the other way around. This should solve it...
                tiledTileset = map.Tilesets.Where(t => t.FirstGid >= tileIndex).OrderByDescending(t => t.FirstGid).FirstOrDefault();
                if (tiledTileset == null)
                {
                    // Still couldn't find the tileset.
                    string message = string.Format("Could not find tileset for tile #{0}.", tileIndex);
                    throw new InvalidOperationException(message);
                }
            }
            return tiledTileset;
        }
        private void DrawTile(TmxLayerTile tileId, float x, float y, bool abs = false)
        {
            TmxTileset tile = GetTileSetForTile(tileId.Gid);
            int width = (int)(tile.Image.Width - (tile.Image.Width % tile.TileWidth)), height = (int)(tile.Image.Height - (tile.Image.Height % tile.TileHeight));
            int tWidth = tile.TileWidth, tHeight = tile.TileHeight;
            int gid = tileId.Gid - tile.FirstGid;
            float xPos = abs ? x : tileId.X * tile.TileWidth - x * tile.TileWidth;
            float yPos = abs ? y : tileId.Y * tile.TileHeight - y * tile.TileHeight;

            EngineCore.SetTexture(TileSets[tile.Name], 0);
            EngineCore.RenderToTexture(xPos, yPos,

                ((gid * tWidth) % width),

                ((gid * tWidth) / width) * tHeight,

                tile.TileWidth, tile.TileHeight);
        }

        public void RemoveTile(string layerName, float x, float y)
        {
            x -= (x % map.TileWidth);
            y -= (y % map.TileHeight);

            x /= map.TileWidth;
            y /= map.TileHeight;

            x += LocalPos.X;
            y += LocalPos.Y;

            try
            {
                int layerID = map.Layers.IndexOf(map.Layers.Single(z => z.Name == layerName));
                map.Layers[layerID].Tiles.Remove(map.Layers[layerID].Tiles.Single(t => t.Gid != 0 && t.X == x && t.Y == y));

                foreach (TmxLayer layer in map.Layers)
                {
                    if (layer.Visible)
                    {
                        foreach (TmxLayerTile tileId in layer.Tiles)
                        {
                            if (tileId.X == x && tileId.Y == y && layer.Visible && tileId.Gid != 0 && !layer.Properties.ContainsKey("Collision") && !layer.Properties.ContainsKey("Pickup"))
                            {
                                DrawTile(tileId, LocalPos.X, LocalPos.Y);
                            }
                        }
                    }
                }

            }
            catch (Exception) { }
        }

        public TmxTilesetTile GetTileWithProperties(float x, float y)
        {
            x -= (x % map.TileWidth);
            y -= (y % map.TileHeight);

            x /= map.TileWidth;
            y /= map.TileHeight;

            x += LocalPos.X;
            y += LocalPos.Y;

            for (int i = map.Layers.Count - 1; i >= 0; i--)
            {
                if (!map.Layers[i].Properties.ContainsKey("Collision"))
                {
                    var t = map.Layers[i].Tiles.SingleOrDefault(z => z.X == x && z.Y == y && z.Gid != 0);
                    if (t != null)
                    {
                        var tileSet = GetTileSetForTile(t.Gid);
                        var tile = tileSet.Tiles.SingleOrDefault(z => z.Id == (t.Gid - tileSet.FirstGid) && z.Properties.ContainsKey("Event"));
                        if (tile != null) return tile;
                    }
                }
            }

            return null;
        }

        public bool Collidable(Vector2 point) { return Collidable((int)point.X, (int)point.Y); }
        public bool Collidable(int x, int y)
        {
            //Calculate the tile X and Y coordinates
            x -= (x % map.TileWidth);
            y -= (y % map.TileHeight);

            x /= map.TileWidth;
            y /= map.TileHeight;

            x += (int)LocalPos.X;
            y += (int)LocalPos.Y;

            var collisionMap = map.Layers.Where(l => l.Properties.ContainsKey("Collision"));

            return collisionMap.Any(z => { return z.Tiles.Exists(t => t.X == x && t.Y == y && t.Gid != 0); });
        }

        public void HideLayer(string name)
        {
            TmxLayer layer = map.Layers.SingleOrDefault(t => t.Name == name);
            layer.Visible = false;
        }

        public void ClearLayer(string name)
        {
            TmxLayer layer = map.Layers.SingleOrDefault(t => t.Name == name);
            layer.Tiles.Clear();
        }

        public void IsDirty(bool isD)
        {
            dirty = isD;
        }
        public void Draw() { Draw(LocalPos); }
        internal void Draw(Vector2 point) { Draw(point.X, point.Y); }
        internal void Draw(float x, float y)
        {
            if (!dirty)
            {
                EngineCore.BindRenderTexture(0);
                EngineCore.DrawRect(0, 0, 0, 0, 960, 540);
                return;
            }

            EngineCore.ClearRenderTexture(0, 0, 0, 255);

            foreach (TmxLayer layer in map.Layers)
            {
                if (layer.Visible)
                {
                    foreach (TmxLayerTile tileId in layer.Tiles)
                    {
                        int tileX = tileId.X * map.TileWidth;
                        int tileY = tileId.Y * map.TileHeight;

                        if (tileX >= x * map.TileWidth && tileX < EngineCore.Screen.X + (x * map.TileWidth) &&
                            tileY >= y * map.TileHeight && tileY < EngineCore.Screen.Y + (y * map.TileHeight) &&
                            layer.Visible && tileId.Gid != 0 && !layer.Properties.ContainsKey("Collision") && !layer.Properties.ContainsKey("Pickup"))
                        {
                            DrawTile(tileId, x, y);
                        }
                        if (tileX > EngineCore.Screen.X + (x * map.TileWidth) && tileY > EngineCore.Screen.Y + (y * map.TileHeight)) break;
                    }
                }
            }


            foreach (TmxObjectGroup obj in map.ObjectGroups)
                foreach (TmxObjectGroup.TmxObject sObj in obj.Objects)
                {
                    if (sObj.X >= x && sObj.X < EngineCore.Screen.X + x &&
                        sObj.Y >= y && sObj.Y < EngineCore.Screen.Y + y &&
                        obj.Visible && sObj.Tile.Gid != 0) DrawTile(sObj.Tile, sObj.X, sObj.Y, true);
                }


            dirty = false;

        }

    }
}
