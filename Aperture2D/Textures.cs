﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperture2D
{
    internal static class Textures
    {
        static Dictionary<string, object> textures;

        static Textures()
        {
            textures = new Dictionary<string, object>();
        }

        internal static void LoadTexture(string path)
        {
            textures[path] = EngineCore.LoadTexture(path);
        }

        internal static object GetTexture(string path)
        {
            if (!textures.ContainsKey(path)) LoadTexture(path);
            return textures[path];
        }

        internal static void SetTexture(string path, object tex)
        {
            textures[path] = tex;
        }

    }
}
