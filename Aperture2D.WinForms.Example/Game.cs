﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aperture2D.WinForms.Example
{
    public class Game : GameWindow
    {
        RPG rpg;
        public Game()
            : base()
        {
            rpg = new RPG("levelA.tmx", "playerBlackAnim.xml");
            rpg.Controller.Speed = 20;
            rpg.Player.Position = new Vector2(40, 40);
            EngineCore.Render = rpg.Draw;

            bool isWhite = false;
            bool prevRed = false;

            rpg.Events["whiteTile"] = (dict) =>
            {
                if (isWhite && !prevRed)
                {
                    this.Close();
                }
                if (prevRed) isWhite = false;
                prevRed = false;
            };

            rpg.Events["redTile"] = (dict) =>
            {
                prevRed = true;
            };

            rpg.Events["blackTile"] = (dict) =>
            {
                if (!isWhite && !prevRed)
                {
                    this.Close();
                }
                if (prevRed) isWhite = true;
                prevRed = false;
            };

            rpg.Events["toggleSwitch"] = (dict) =>
            {
                rpg.Map.HideLayer("block");
                rpg.Map.ClearLayer("Tile Layer 3");
                rpg.Map.IsDirty(true);
            };

            rpg.Events["pickUp"] = (dict) =>
                {
                    rpg.Map.RemoveTile("pickups", rpg.Player.Position.X + 1, rpg.Player.Position.Y + 1);
                    //rpg.Map.IsDirty(true);
                };

            rpg.Events["levelComplete"] = (dict) =>
            {
                this.Close();
            };

        }

    }
}
